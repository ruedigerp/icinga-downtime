## downtime.conf

    cp downtime.conf.example downtime.conf

edit downtime.conf

    apihost="monitoring.company.net"
    apiuser="username"
    apipass="secret"

## icinga2 api-users.conf 

    object ApiUser "username" {
      password = "secret"

      permissions = [ "actions/schedule-downtime" ]
    }

icinga reload

## usage downtime.sh 

### set downtime

set downtime for host example.com and all services. Duration = 1 h 

    ./downtime.sh -h example.com -d 60 -c "Service migration to other cluster" 

### list all downtimes

    ./downtime.sh -l 

or pretty output:

    ./downtime.sh -l | jq 

### remove downtime

    ./downtime.sh -h example.com -r 


