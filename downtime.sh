#!/bin/bash

print_usage(){
    echo "options:"
    echo "-h        hostname"
    echo "-d        duration in minutes"
    echo "-c        comment"
    echo "-l        list all downtimes"
    echo "-r        remove downtimes"
}

while getopts ':h:d:c:lr' flag; do
  case "${flag}" in
    h) 
      domain="$OPTARG" 
    ;;
    d) 
      duration="$OPTARG"; 
    ;;
    c) 
      comment="$OPTARG" 
    ;;
    l)
      list="list"
    ;;
    r)
      remove="remove"
    ;;
    *) print_usage
    exit 1 ;;
    esac
done
if [ $OPTIND -eq 1 ]; then 
    echo "No options were passed"; 
    print_usage
    exit 1;
fi
shift $((OPTIND -1))

now=$(date +%s); 
von=$(printf "%.f\n" $now)
duration=$(printf "%.d\n" $duration)
bis=$((von+duration*60))
. ./downtime.conf

if [ "$list" == "list" ] 
then
  curl -k -s -u ${apiuser}:${apipass} -H 'Accept: application/json' "https://$apihost/v1/objects/downtimes"
  exit
fi
if [ "$remove" == "remove" ] 
then
  curl -k -s -u ${apiuser}:${apipass} -H 'Accept: application/json' -X POST "https://$apihost/v1/actions/remove-downtime?type=Service&filter=host.name==%22${domain}%22"
  curl -k -s -u ${apiuser}:${apipass} -H 'Accept: application/json' -X POST "https://$apihost/v1/actions/remove-downtime?type=Host&filter=host.name==%22${domain}%22"
  exit
fi

echo "{ \"start_time\": $von, \"end_time\": \"$bis\", \"duration\": 1000, \"author\": \"$USER $HOST\", \"comment\": \"$comment\", \"pretty\": true }" > downtime.json

curl -k -s -u ${apiuser}:${apipass} -H 'Accept: application/json' -X POST "https://$apihost/v1/actions/schedule-downtime?type=Service&filter=host.name==%22${domain}%22" -d @downtime.json 

curl -k -s -u ${apiuser}:${apipass} -H 'Accept: application/json' -X POST "https://$apihost/v1/actions/schedule-downtime?type=Host&filter=host.name==%22${domain}%22" -d @downtime.json 



